from django.db import models

# Create your models here.
class Coba(models.Model):
    status = models.CharField(max_length=300)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.status