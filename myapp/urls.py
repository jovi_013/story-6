from django.urls import path
from . import views

app_name = 'myapp'

urlpatterns = [
    path('', views.cobaTest, name='cobaTest'),
    path('delete/<id>', views.delete, name='delete'),

]