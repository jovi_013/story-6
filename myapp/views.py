from django.shortcuts import render, redirect
from .forms import BuatStatus
from .models import Coba
from . import forms
from django.http import HttpResponseRedirect

# Create your views here.

def cobaTest(request):
    data = Coba.objects.all()
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = BuatStatus(request.POST or None)
        # check whether it's valid:
        if form.is_valid():
            form.save()
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            return HttpResponseRedirect('/')
    
    # if a GET (or any other method) we'll create a blank form
    else:
        form = BuatStatus()

    return render(request, 'myapp/landing_page.html', {'form':form, 'data':data})

def delete(request, id):
    data = Coba.objects.get(id=id)
    data.delete()
    return HttpResponseRedirect('/')
