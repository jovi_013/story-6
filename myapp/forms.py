from django import forms
from .models import Coba


class BuatStatus(forms.ModelForm):
    status = forms.CharField(widget=forms.Textarea, max_length=300)
    class Meta():
        model = Coba
        fields = ['status']
        