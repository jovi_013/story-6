from django.test import TestCase, LiveServerTestCase
from django.http import HttpRequest
from django.urls import reverse, resolve
from .models import Coba
from .views import cobaTest

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import time
import chromedriver_binary

# Create your tests here.
class myAppTest(TestCase):
    
    def test_url_exist(self):
        response = self.client.get("/")
        self.assertEqual(response.status_code, 200)
    
    def test_url_not_exist(self):
        response = self.client.get("/ngulang_ppw/")
        self.assertEqual(response.status_code, 404)
    
    def test_template_benar(self):
        response = self.client.get(reverse('myapp:cobaTest'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'myapp/landing_page.html')
    
    def test_header_exist(self):
        response = self.client.get('/')
        isi_html = response.content.decode("utf8")
        self.assertIn("Halo, apa kabar?", isi_html)
    
    def test_button_submit_exist(self):
        response = self.client.get('/')
        isi_html = response.content.decode("utf8")
        self.assertIn("<input", isi_html)
        self.assertIn('type="submit"', isi_html)
    
    def test_landingPage_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, cobaTest)

    def test_post_and_saved(self):
        self.client.post('/', {'status': 'test'})
        self.assertEqual(True, Coba.objects.filter(status='test').exists())
    
    def test_model(self):
        Coba.objects.create(status = "test juga")
        count = Coba.objects.all().count()
        self.assertEqual(1, count)

    def test_field(self):
        tess = 'a'*400
        self.client.post('/', {'status': tess})
        self.assertEqual(False, Coba.objects.filter(status=tess).exists())
    
    def test_form_exist(self):
        response = self.client.get('/')
        isi_html = response.content.decode("utf8")
        self.assertIn("<form", isi_html)

class functionalTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.driver = webdriver.Chrome(chrome_options=chrome_options)

    def tearDown(self):
        self.driver.quit()
    
    def test_input(self):
        self.driver.get(self.live_server_url)
        msg = self.driver.find_element_by_id('id_status')
        send = self.driver.find_element_by_id('button_submit')
        msg.send_keys('Coba Coba')
        send.send_keys(Keys.RETURN)
        results = self.driver.find_element_by_class_name('status_list').text
        self.assertIn('Coba Coba', results)
    
    def test_300_max_length(self):
        self.driver.get(self.live_server_url)
        msg = self.driver.find_element_by_id('id_status')
        send = self.driver.find_element_by_id('button_submit')
        msg.send_keys('a'*400)
        send.send_keys(Keys.RETURN)
        results = self.driver.find_element_by_class_name('status_list').text
        self.maxDiff = None
        self.assertEqual(len(results), 300)









