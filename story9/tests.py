from django.test import TestCase, LiveServerTestCase
from django.http import HttpRequest
from django.urls import reverse, resolve
from .views import story9_index

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import time
import chromedriver_binary
from django.contrib.auth import get_user_model
# from django.contrib.auth import views as auth_views
# from django.contrib.auth.views import LoginView


# Create your tests here.
class myAppTest(TestCase):
    
    def test_url_exist(self):
        response = self.client.get("/story9/")
        self.assertEqual(response.status_code, 200)
    
    def test_url_not_exist(self):
        response = self.client.get("/ngulang_ppw/")
        self.assertEqual(response.status_code, 404)
    
    def test_template_benar(self):
        response = self.client.get(reverse('story9:story9_index'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'story9/story9_index.html')
    
    # def test_page_using_index_func(self):
    #     found = resolve('/story9/')
    #     self.assertEqual(found.func, LoginView)

class FunctionalTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.driver = webdriver.Chrome(chrome_options=chrome_options)

    def tearDown(self):
        self.driver.quit()
    
    def test_open(self):
        self.driver.get("http://localhost:8000/story9/")

class AuthTest(TestCase):

    def setUp(self):
        User = get_user_model()
        user = User.objects.create_user('temporary', 'temporary@gmail.com', 'temporary')

    def test_login_redirect(self):
        response = self.client.get('/story9/profile', follow=True)
        self.assertContains(response, 'Please login')

    def test_secure_page(self):
        User = get_user_model()
        self.client.login(username='temporary', password='temporary')
        response = self.client.get('/story9/profile/', follow=True)
        self.assertContains(response, 'temporary')