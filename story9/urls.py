from django.urls import path
from . import views
from django.contrib.auth import views as auth_views

app_name = 'story9'

urlpatterns = [
    path('', auth_views.LoginView.as_view(template_name="story9/story9_index.html", redirect_authenticated_user=True), name='story9_index'),
    path('login/', auth_views.LoginView.as_view(template_name="story9/login.html", redirect_authenticated_user=True), name="login"),
    path('profile/', views.profile, name='profile'),
    path('logout/', auth_views.LogoutView.as_view(template_name="story9/logout.html"), name="logout")

]