from django.shortcuts import render
from django.contrib.auth.decorators import login_required

# Create your views here.
def story9_index(request):
    return render(request, "story9/story9_index.html")

@login_required
def profile(request):
    return render(request, "story9/profile_story9.html")
    