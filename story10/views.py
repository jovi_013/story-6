from django.shortcuts import render

# Create your views here.
def story10_index(request):
    return render(request, "story10/story10_index.html")