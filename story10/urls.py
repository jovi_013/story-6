from django.urls import path
from . import views

app_name = 'story10'

urlpatterns = [
    path('', views.story10_index, name='story10_index')

]