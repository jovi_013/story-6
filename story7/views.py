from django.shortcuts import render

# Create your views here.
def story7_index(request):
    return render(request, "story7/story7_index.html")
