from django.test import TestCase, LiveServerTestCase
from django.http import HttpRequest
from django.urls import reverse, resolve
from .views import story7_index

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import time
import chromedriver_binary

# Create your tests here.
class myAppTest(TestCase):
    
    def test_url_exist(self):
        response = self.client.get("/story7/")
        self.assertEqual(response.status_code, 200)
    
    def test_url_not_exist(self):
        response = self.client.get("/ngulang_ppw/")
        self.assertEqual(response.status_code, 404)
    
    def test_template_benar(self):
        response = self.client.get(reverse('story7:story7_index'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'story7/story7_index.html')
    
    def test_accordion_menu_exist(self):
        response = self.client.get('/story7/')
        isi_html = response.content.decode("utf8")
        self.assertIn("Aktivitas", isi_html)
        self.assertIn("Organisasi", isi_html)
        self.assertIn("Kepanitiaan", isi_html)
        self.assertIn("Prestasi", isi_html)
    
    def test_button_ubahTema_exist(self):
        response = self.client.get('/story7/')
        isi_html = response.content.decode("utf8")
        self.assertIn("Ubah Tema", isi_html)
        self.assertIn("<button", isi_html)
        self.assertIn('id="ubah"', isi_html)
    
    def test_page_using_index_func(self):
        found = resolve('/story7/')
        self.assertEqual(found.func, story7_index)

class functionalTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.driver = webdriver.Chrome(chrome_options=chrome_options)

    def tearDown(self):
        self.driver.quit()
    
    def test_fungsi_tombol_ubahTema(self):
        self.driver.get("http://localhost:8000/story7/")
        ubah = self.driver.find_element_by_id("ubah")
        body = self.driver.find_element_by_tag_name("body")
        
        self.assertEqual(body.value_of_css_property("backgroundColor"), 'rgba(255, 255, 255, 1)')
        ubah.click()
        time.sleep(2)
        self.assertEqual(body.value_of_css_property("backgroundColor"), 'rgba(48, 48, 48, 1)')








