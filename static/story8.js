async function doSearch(q) {
    const queryUrl = `https://www.googleapis.com/books/v1/volumes?q=${q}`;

    const data = await (await fetch(queryUrl)).json();

    const result = data.items;

    if(data) {
        $("#cont").html(result.map((item) => {
            const {title, authors, imageLinks, canonicalVolumeLink} = item.volumeInfo;
            return `
            <tr>
                <td>${imageLinks ? `<img src=${imageLinks.thumbnail} />` : `No Image`}</td>
                <td>${title}</td>
                <td>${authors ? authors.join(", ") : "Unknown"}</td>
                <td><a href=${canonicalVolumeLink}>Link</a></td>
            </tr>
            `;
        }));
    }
    else {
        $("#cont").html("Tidak ditemukan!");
    }
}


$("document").ready(function() {
    doSearch("Doraemon");
    $("#input-form").submit(async (e) => {
        e.preventDefault();

        $("#cont").html("Loading...");

        const input = $("#searchbox").val();
        doSearch(input)
    })
});