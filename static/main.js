
$(document).ready(function () {

  let lightTheme = true;

  $("#ubah").click(function () {
    lightTheme = !lightTheme;
    if (lightTheme === true) {
      $("body").css("background-color", "white")
      $("body").css("transition", "1s")
      $("body").css("color", "black")
    }
    else {
      $("body").css("background-color", "#303030")
      $("body").css("transition", "1s")
      $("body").css("color", "white")
    }
  });
});

$('.menu').on('click', function () {
  $('.content').slideUp();
  const el = $(this).children('.content');
  el.slideToggle();
});
