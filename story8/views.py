from django.shortcuts import render

# Create your views here.
def story8_index(request):
    return render(request, "story8/story8_index.html")
