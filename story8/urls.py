from django.urls import path
from . import views

app_name = 'story8'

urlpatterns = [
    path('', views.story8_index, name='story8_index'),

]