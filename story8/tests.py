from django.test import TestCase, LiveServerTestCase
from django.http import HttpRequest
from django.urls import reverse, resolve
from .views import story8_index

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import time
import chromedriver_binary

# Create your tests here.
class myAppTest(TestCase):
    
    def test_url_exist(self):
        response = self.client.get("/story8/")
        self.assertEqual(response.status_code, 200)
    
    def test_url_not_exist(self):
        response = self.client.get("/ngulang_ppw/")
        self.assertEqual(response.status_code, 404)
    
    def test_template_benar(self):
        response = self.client.get(reverse('story8:story8_index'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'story8/story8_index.html')
    
    def test_button_search_exist(self):
        response = self.client.get('/story8/')
        isi_html = response.content.decode("utf8")
        self.assertIn('value="Search"', isi_html)
        self.assertIn("<input", isi_html)
        self.assertIn('id="cari"', isi_html)
    
    def test_page_using_index_func(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, story8_index)

    def test_using_right_staticfiles(self):
        response = self.client.get('/story8/')
        self.assertContains(response, 'static/story8')

class functionalTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.driver = webdriver.Chrome(chrome_options=chrome_options)

    def tearDown(self):
        self.driver.quit()
    
    def test_open(self):
        self.driver.get("http://localhost:8000/story8/")

    







